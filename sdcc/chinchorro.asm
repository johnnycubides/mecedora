;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.5.0 #9253 (Jun 20 2015) (Linux)
; This file was generated Thu Sep 24 12:04:22 2015
;--------------------------------------------------------
; PIC port for the 14-bit core
;--------------------------------------------------------
;	.file	"chinchorro.c"
	list	p=16f628a
	radix dec
	include "p16f628a.inc"
;--------------------------------------------------------
; config word(s)
;--------------------------------------------------------
	__config 0x2118
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	extern	_STATUSbits
	extern	_PORTAbits
	extern	_PORTBbits
	extern	_INTCONbits
	extern	_PIR1bits
	extern	_T1CONbits
	extern	_T2CONbits
	extern	_CCP1CONbits
	extern	_RCSTAbits
	extern	_CMCONbits
	extern	_OPTION_REGbits
	extern	_TRISAbits
	extern	_TRISBbits
	extern	_PIE1bits
	extern	_PCONbits
	extern	_TXSTAbits
	extern	_EECON1bits
	extern	_VRCONbits
	extern	_INDF
	extern	_TMR0
	extern	_PCL
	extern	_STATUS
	extern	_FSR
	extern	_PORTA
	extern	_PORTB
	extern	_PCLATH
	extern	_INTCON
	extern	_PIR1
	extern	_TMR1
	extern	_TMR1L
	extern	_TMR1H
	extern	_T1CON
	extern	_TMR2
	extern	_T2CON
	extern	_CCPR1
	extern	_CCPR1L
	extern	_CCPR1H
	extern	_CCP1CON
	extern	_RCSTA
	extern	_TXREG
	extern	_RCREG
	extern	_CMCON
	extern	_OPTION_REG
	extern	_TRISA
	extern	_TRISB
	extern	_PIE1
	extern	_PCON
	extern	_PR2
	extern	_TXSTA
	extern	_SPBRG
	extern	_EEDATA
	extern	_EEADR
	extern	_EECON1
	extern	_EECON2
	extern	_VRCON
	extern	__sdcc_gsinit_startup
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	global	_delay
	global	_luces
	global	_main

	global PSAVE
	global SSAVE
	global WSAVE
	global STK12
	global STK11
	global STK10
	global STK09
	global STK08
	global STK07
	global STK06
	global STK05
	global STK04
	global STK03
	global STK02
	global STK01
	global STK00

sharebank udata_ovr 0x0070
PSAVE	res 1
SSAVE	res 1
WSAVE	res 1
STK12	res 1
STK11	res 1
STK10	res 1
STK09	res 1
STK08	res 1
STK07	res 1
STK06	res 1
STK05	res 1
STK04	res 1
STK03	res 1
STK02	res 1
STK01	res 1
STK00	res 1

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
UDL_chinchorro_0	udata
r0x100A	res	1
r0x1004	res	1
r0x1005	res	1
r0x1006	res	1
r0x1007	res	1
r0x1008	res	1
r0x1009	res	1
r0x1001	res	1
r0x1000	res	1
r0x1002	res	1
r0x1003	res	1
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; reset vector 
;--------------------------------------------------------
STARTUP	code 0x0000
	nop
	pagesel __sdcc_gsinit_startup
	goto	__sdcc_gsinit_startup
;--------------------------------------------------------
; code
;--------------------------------------------------------
code_chinchorro	code
;***
;  pBlock Stats: dbName = M
;***
;entry:  _main	;Function start
; 2 exit points
;has an exit
;2 compiler assigned registers:
;   r0x100A
;   r0x100B
;; Starting pCode block
_main	;Function start
; 2 exit points
;	.line	13; "chinchorro.c"	TRISA = 0x07;
	MOVLW	0x07
	BANKSEL	_TRISA
	MOVWF	_TRISA
;	.line	14; "chinchorro.c"	TRISB = 0x00;
	CLRF	_TRISB
;	.line	15; "chinchorro.c"	PORTB = 0;
	BANKSEL	_PORTB
	CLRF	_PORTB
_00115_DS_
;	.line	17; "chinchorro.c"	if(RA0 == 1){       //motor y luces on
	BANKSEL	r0x100A
	CLRF	r0x100A
	BANKSEL	_PORTAbits
	BTFSS	_PORTAbits,0
	GOTO	_00001_DS_
	BANKSEL	r0x100A
	INCF	r0x100A,F
_00001_DS_
	BANKSEL	r0x100A
	MOVF	r0x100A,W
;;1	MOVWF	r0x100B
	XORLW	0x01
	BTFSS	STATUS,2
	GOTO	_00112_DS_
;	.line	18; "chinchorro.c"	PORTB = 1;
	MOVLW	0x01
	BANKSEL	_PORTB
	MOVWF	_PORTB
	GOTO	_00115_DS_
_00112_DS_
;	.line	21; "chinchorro.c"	else if(RA1 == 0){  //Motor on
	BANKSEL	_PORTAbits
	BTFSC	_PORTAbits,1
	GOTO	_00109_DS_
;	.line	22; "chinchorro.c"	PORTB = 2;           
	MOVLW	0x02
	MOVWF	_PORTB
	GOTO	_00115_DS_
_00109_DS_
;	.line	24; "chinchorro.c"	else if(RA2 == 0){  //Luces on
	BANKSEL	_PORTAbits
	BTFSC	_PORTAbits,2
	GOTO	_00106_DS_
;	.line	26; "chinchorro.c"	PORTB = 4;
	MOVLW	0x04
	MOVWF	_PORTB
	GOTO	_00115_DS_
_00106_DS_
;	.line	29; "chinchorro.c"	PORTB = 8;      //all off
	MOVLW	0x08
	BANKSEL	_PORTB
	MOVWF	_PORTB
	GOTO	_00115_DS_
	RETURN	
; exit point of _main

;***
;  pBlock Stats: dbName = C
;***
;entry:  _delay	;Function start
; 2 exit points
;has an exit
;5 compiler assigned registers:
;   r0x1000
;   STK00
;   r0x1001
;   r0x1002
;   r0x1003
;; Starting pCode block
_delay	;Function start
; 2 exit points
;	.line	71; "chinchorro.c"	void delay(uint16_t time){
	BANKSEL	r0x1000
	MOVWF	r0x1000
	MOVF	STK00,W
	MOVWF	r0x1001
;	.line	73; "chinchorro.c"	for(i = 0; i < time; i++);
	CLRF	r0x1002
	CLRF	r0x1003
_00156_DS_
	BANKSEL	r0x1000
	MOVF	r0x1000,W
	SUBWF	r0x1003,W
	BTFSS	STATUS,2
	GOTO	_00207_DS_
	MOVF	r0x1001,W
	SUBWF	r0x1002,W
_00207_DS_
	BTFSC	STATUS,0
	GOTO	_00150_DS_
;;genSkipc:3247: created from rifx:0xbfef4124
	BANKSEL	r0x1002
	INCF	r0x1002,F
	BTFSC	STATUS,2
	INCF	r0x1003,F
	GOTO	_00156_DS_
_00150_DS_
;	.line	74; "chinchorro.c"	for(i = 0; i < time; i++);
	BANKSEL	r0x1002
	CLRF	r0x1002
	CLRF	r0x1003
_00159_DS_
	BANKSEL	r0x1000
	MOVF	r0x1000,W
	SUBWF	r0x1003,W
	BTFSS	STATUS,2
	GOTO	_00208_DS_
	MOVF	r0x1001,W
	SUBWF	r0x1002,W
_00208_DS_
	BTFSC	STATUS,0
	GOTO	_00151_DS_
;;genSkipc:3247: created from rifx:0xbfef4124
	BANKSEL	r0x1002
	INCF	r0x1002,F
	BTFSC	STATUS,2
	INCF	r0x1003,F
	GOTO	_00159_DS_
_00151_DS_
;	.line	75; "chinchorro.c"	for(i = 0; i < time; i++);
	BANKSEL	r0x1002
	CLRF	r0x1002
	CLRF	r0x1003
_00162_DS_
	BANKSEL	r0x1000
	MOVF	r0x1000,W
	SUBWF	r0x1003,W
	BTFSS	STATUS,2
	GOTO	_00209_DS_
	MOVF	r0x1001,W
	SUBWF	r0x1002,W
_00209_DS_
	BTFSC	STATUS,0
	GOTO	_00152_DS_
;;genSkipc:3247: created from rifx:0xbfef4124
	BANKSEL	r0x1002
	INCF	r0x1002,F
	BTFSC	STATUS,2
	INCF	r0x1003,F
	GOTO	_00162_DS_
_00152_DS_
;	.line	76; "chinchorro.c"	for(i = 0; i < time; i++);
	BANKSEL	r0x1002
	CLRF	r0x1002
	CLRF	r0x1003
_00165_DS_
	BANKSEL	r0x1000
	MOVF	r0x1000,W
	SUBWF	r0x1003,W
	BTFSS	STATUS,2
	GOTO	_00210_DS_
	MOVF	r0x1001,W
	SUBWF	r0x1002,W
_00210_DS_
	BTFSC	STATUS,0
	GOTO	_00153_DS_
;;genSkipc:3247: created from rifx:0xbfef4124
	BANKSEL	r0x1002
	INCF	r0x1002,F
	BTFSC	STATUS,2
	INCF	r0x1003,F
	GOTO	_00165_DS_
_00153_DS_
;	.line	77; "chinchorro.c"	for(i = 0; i < time; i++);
	BANKSEL	r0x1002
	CLRF	r0x1002
	CLRF	r0x1003
_00168_DS_
	BANKSEL	r0x1000
	MOVF	r0x1000,W
	SUBWF	r0x1003,W
	BTFSS	STATUS,2
	GOTO	_00211_DS_
	MOVF	r0x1001,W
	SUBWF	r0x1002,W
_00211_DS_
	BTFSC	STATUS,0
	GOTO	_00170_DS_
;;genSkipc:3247: created from rifx:0xbfef4124
	BANKSEL	r0x1002
	INCF	r0x1002,F
	BTFSC	STATUS,2
	INCF	r0x1003,F
	GOTO	_00168_DS_
_00170_DS_
	RETURN	
; exit point of _delay

;***
;  pBlock Stats: dbName = C
;***
;entry:  _luces	;Function start
; 2 exit points
;has an exit
;functions called:
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;   _delay
;7 compiler assigned registers:
;   r0x1004
;   r0x1005
;   r0x1006
;   r0x1007
;   r0x1008
;   STK00
;   r0x1009
;; Starting pCode block
_luces	;Function start
; 2 exit points
;	.line	34; "chinchorro.c"	uint8_t luces(char offset){
	BANKSEL	r0x1004
	MOVWF	r0x1004
;	.line	36; "chinchorro.c"	while(1){
	INCF	r0x1004,W
	MOVWF	r0x1005
	MOVLW	0x02
	ADDWF	r0x1004,W
	MOVWF	r0x1006
	MOVLW	0x04
	ADDWF	r0x1004,W
	MOVWF	r0x1007
_00145_DS_
;	.line	37; "chinchorro.c"	for(j = 0; j < 50; j++){
	BANKSEL	r0x1008
	CLRF	r0x1008
_00135_DS_
;	.line	38; "chinchorro.c"	PORTB = offset + 1;
	BANKSEL	r0x1005
	MOVF	r0x1005,W
	BANKSEL	_PORTB
	MOVWF	_PORTB
;	.line	39; "chinchorro.c"	delay(RETARDO);
	MOVLW	0x10
	MOVWF	STK00
	MOVLW	0x27
	PAGESEL	_delay
	CALL	_delay
	PAGESEL	$
;	.line	40; "chinchorro.c"	PORTB = offset + 2;
	BANKSEL	r0x1006
	MOVF	r0x1006,W
	BANKSEL	_PORTB
	MOVWF	_PORTB
;	.line	41; "chinchorro.c"	delay(RETARDO);
	MOVLW	0x10
	MOVWF	STK00
	MOVLW	0x27
	PAGESEL	_delay
	CALL	_delay
	PAGESEL	$
;	.line	42; "chinchorro.c"	PORTB = offset + 4;
	BANKSEL	r0x1007
	MOVF	r0x1007,W
	BANKSEL	_PORTB
	MOVWF	_PORTB
;	.line	43; "chinchorro.c"	delay(RETARDO);
	MOVLW	0x10
	MOVWF	STK00
	MOVLW	0x27
	PAGESEL	_delay
	CALL	_delay
	PAGESEL	$
;	.line	44; "chinchorro.c"	if( !(RA0 == 0 || RA2 == 0)){
	BANKSEL	_PORTAbits
	BTFSS	_PORTAbits,0
	GOTO	_00136_DS_
	BTFSS	_PORTAbits,2
	GOTO	_00136_DS_
;	.line	45; "chinchorro.c"	return 1;
	MOVLW	0x01
	GOTO	_00141_DS_
_00136_DS_
;	.line	37; "chinchorro.c"	for(j = 0; j < 50; j++){
	BANKSEL	r0x1008
	INCF	r0x1008,F
;;unsigned compare: left < lit(0x32=50), size=1
	MOVLW	0x32
	SUBWF	r0x1008,W
	BTFSS	STATUS,0
	GOTO	_00135_DS_
;;genSkipc:3247: created from rifx:0xbfef4124
;	.line	48; "chinchorro.c"	for(j = 0; j < 50; j++){
	CLRF	r0x1008
_00137_DS_
;	.line	49; "chinchorro.c"	PORTB = offset + 4;
	BANKSEL	r0x1007
	MOVF	r0x1007,W
	BANKSEL	_PORTB
	MOVWF	_PORTB
;	.line	50; "chinchorro.c"	delay(RETARDO);
	MOVLW	0x10
	MOVWF	STK00
	MOVLW	0x27
	PAGESEL	_delay
	CALL	_delay
	PAGESEL	$
;	.line	51; "chinchorro.c"	PORTB = offset + 2;
	BANKSEL	r0x1006
	MOVF	r0x1006,W
	BANKSEL	_PORTB
	MOVWF	_PORTB
;	.line	52; "chinchorro.c"	delay(RETARDO);
	MOVLW	0x10
	MOVWF	STK00
	MOVLW	0x27
	PAGESEL	_delay
	CALL	_delay
	PAGESEL	$
;	.line	53; "chinchorro.c"	PORTB = offset + 1;
	BANKSEL	r0x1005
	MOVF	r0x1005,W
	BANKSEL	_PORTB
	MOVWF	_PORTB
;	.line	54; "chinchorro.c"	delay(RETARDO);
	MOVLW	0x10
	MOVWF	STK00
	MOVLW	0x27
	PAGESEL	_delay
	CALL	_delay
	PAGESEL	$
;	.line	55; "chinchorro.c"	if( !(RA0 == 0 || RA2 == 0)){
	BANKSEL	_PORTAbits
	BTFSS	_PORTAbits,0
	GOTO	_00138_DS_
	BTFSS	_PORTAbits,2
	GOTO	_00138_DS_
;	.line	56; "chinchorro.c"	return 1;
	MOVLW	0x01
	GOTO	_00141_DS_
_00138_DS_
;	.line	48; "chinchorro.c"	for(j = 0; j < 50; j++){
	BANKSEL	r0x1008
	INCF	r0x1008,F
;;unsigned compare: left < lit(0x32=50), size=1
	MOVLW	0x32
	SUBWF	r0x1008,W
	BTFSS	STATUS,0
	GOTO	_00137_DS_
;;genSkipc:3247: created from rifx:0xbfef4124
;	.line	59; "chinchorro.c"	for(j = 0; j < 50; j++){
	MOVLW	0x07
	ADDWF	r0x1004,W
	MOVWF	r0x1008
	CLRF	r0x1009
_00139_DS_
;	.line	60; "chinchorro.c"	PORTB = offset + 0;
	BANKSEL	r0x1004
	MOVF	r0x1004,W
	BANKSEL	_PORTB
	MOVWF	_PORTB
;	.line	61; "chinchorro.c"	delay(RETARDO);
	MOVLW	0x10
	MOVWF	STK00
	MOVLW	0x27
	PAGESEL	_delay
	CALL	_delay
	PAGESEL	$
;	.line	62; "chinchorro.c"	PORTB = offset + 7;
	BANKSEL	r0x1008
	MOVF	r0x1008,W
	BANKSEL	_PORTB
	MOVWF	_PORTB
;	.line	63; "chinchorro.c"	delay(RETARDO);
	MOVLW	0x10
	MOVWF	STK00
	MOVLW	0x27
	PAGESEL	_delay
	CALL	_delay
	PAGESEL	$
;	.line	64; "chinchorro.c"	if( !(RA0 == 0 || RA2 == 0)){
	BANKSEL	_PORTAbits
	BTFSS	_PORTAbits,0
	GOTO	_00140_DS_
	BTFSS	_PORTAbits,2
	GOTO	_00140_DS_
;	.line	65; "chinchorro.c"	return 1;
	MOVLW	0x01
	GOTO	_00141_DS_
_00140_DS_
;	.line	59; "chinchorro.c"	for(j = 0; j < 50; j++){
	BANKSEL	r0x1009
	INCF	r0x1009,F
;;unsigned compare: left < lit(0x32=50), size=1
	MOVLW	0x32
	SUBWF	r0x1009,W
	BTFSS	STATUS,0
	GOTO	_00139_DS_
;;genSkipc:3247: created from rifx:0xbfef4124
	GOTO	_00145_DS_
_00141_DS_
	RETURN	
; exit point of _luces


;	code size estimation:
;	  201+   65 =   266 instructions (  662 byte)

	end

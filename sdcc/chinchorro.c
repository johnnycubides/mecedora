#include <pic16f628a.h>
#include <stdint.h>

static __code uint16_t __at (0x2007) configword1 = 0x2118;

#define RETARDO 10000

void delay(uint16_t time);
uint8_t luces(char offset);


void main(void){
    TRISA = 0x07;
    TRISB = 0x00;
    PORTB = 0;
    while(1){
        if(RA0 == 1){       //motor y luces on
            PORTB = 1;
            //luces(8);
        }
        else if(RA1 == 0){  //Motor on
            PORTB = 2;           
        }
        else if(RA2 == 0){  //Luces on
            //luces(0);
            PORTB = 4;
        }
        else{
            PORTB = 8;      //all off
        }
    }
}

uint8_t luces(char offset){
    uint8_t j;
    while(1){
        for(j = 0; j < 50; j++){
            PORTB = offset + 1;
            delay(RETARDO);
            PORTB = offset + 2;
            delay(RETARDO);
            PORTB = offset + 4;
            delay(RETARDO);
            if( !(RA0 == 0 || RA2 == 0)){
                return 1;
            }
        }
        for(j = 0; j < 50; j++){
            PORTB = offset + 4;
            delay(RETARDO);
            PORTB = offset + 2;
            delay(RETARDO);
            PORTB = offset + 1;
            delay(RETARDO);
            if( !(RA0 == 0 || RA2 == 0)){
                return 1;
            }
        }
        for(j = 0; j < 50; j++){
            PORTB = offset + 0;
            delay(RETARDO);
            PORTB = offset + 7;
            delay(RETARDO);
            if( !(RA0 == 0 || RA2 == 0)){
                return 1;
            }
        }
    }
}

void delay(uint16_t time){
    uint16_t i;
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
}

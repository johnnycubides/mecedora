#include <pic16f628a.h>
#include <stdint.h>

static __code uint16_t __at (0x2007) configword1 = 0x2118;

#define RETARDO 10000

void delay(uint16_t time);
uint8_t luces(char offset);


void main(void){
    TRISA = 0x00;
    TRISB = 0xFF;
    PORTA = 0;
    while(1){
        if(RB0 == 0){       //motor y luces on
            luces(8);
        }
        else if(RB1 == 0){  //Motor on
            PORTA = 8;           
        }
        else if(RB2 == 0){  //Luces on
            luces(0);
        }
        else{
            PORTA = 0;      //all off
        }
    }
}

uint8_t luces(char offset){
    uint8_t j;
    while(1){
        for(j = 0; j < 50; j++){
            PORTA = offset + 1;
            delay(RETARDO);
            PORTA = offset + 2;
            delay(RETARDO);
            PORTA = offset + 4;
            delay(RETARDO);
            if( !(RB0 == 0 || RB2 == 0)){
                return 1;
            }
        }
        for(j = 0; j < 50; j++){
            PORTA = offset + 4;
            delay(RETARDO);
            PORTA = offset + 2;
            delay(RETARDO);
            PORTA = offset + 1;
            delay(RETARDO);
            if( !(RB0 == 0 || RB2 == 0)){
                return 1;
            }
        }
        for(j = 0; j < 50; j++){
            PORTA = offset + 0;
            delay(RETARDO);
            PORTA = offset + 7;
            delay(RETARDO);
            if( !(RB0 == 0 || RB2 == 0)){
                return 1;
            }
        }
    }
}

void delay(uint16_t time){
    uint16_t i;
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
}

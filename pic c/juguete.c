// Por ahora para compilar se debe hacer en linux:
// wine Ccsc.exe archivo.c
//para subir el archivo.hex es necesario hacerlo desde dónde se
//la aplicación pk2cmd con el siguiente comando:
//pk2cmd -p -m -farchivo.hex
// podría en primer lugar probar si está conectado el pikit clone
//pk2cmd -?v
//Cristal oscilador interno INTRC_IO
//Crsital oscilador externo
//Sin master clear NOMCLR

#include <16f628a.h>
#fuses XT, NOWDT, NOPROTECT, NOLVP, PUT, BROWNOUT, NOMCLR
#use delay(clock=4000000)
#use standard_io(a)
#use standard_io(b)

#byte PORTB = 0x06
#define RETARDO 8000
#define umbral	30
#define RETARDO_SHORT	4000

void delay(long time);
int luces(char offset);

void main() {
	set_tris_a(0xff);
	set_tris_b(0x00);
	while(TRUE){
		if(input(PIN_A0) == 0){       //motor y luces on
            luces(8);
        }
        else if(input(PIN_A1) == 0){  //Motor on
            PORTB = 8;           
        }
        else if(input(PIN_A2) == 0){  //Luces on
            luces(0);
        }
        else{
            PORTB = 0;      //all off
        }
	}

}

int luces(char offset){
    int j;
    while(1){
        for(j = 0; j < umbral; j++){
            PORTB = offset + 1;
            delay(RETARDO);
            PORTB = offset + 2;
            delay(RETARDO);
            PORTB = offset + 4;
            delay(RETARDO);
            if( !(input(PIN_A0) == 0 || input(PIN_A2) == 0)){
                return 1;
            }
        }
        for(j = 0; j < umbral; j++){
            PORTB = offset + 4;
            delay(RETARDO);
            PORTB = offset + 2;
            delay(RETARDO);
            PORTB = offset + 1;
            delay(RETARDO);
            if( !(input(PIN_A0) == 0 || input(PIN_A2) == 0)){
                return 1;
            }
        }
        for(j = 0; j < umbral; j++){
            PORTB = offset + 0;
            delay(RETARDO);
            PORTB = offset + 7;
            delay(RETARDO);
            if( !(input(PIN_A0) == 0 || input(PIN_A2) == 0)){
                return 1;
            }
        }
        for(j = 0; j < umbral; j++){
            PORTB = offset + 3;
            delay(RETARDO);
            PORTB = offset + 6;
            delay(RETARDO);
            PORTB = offset + 5;
            delay(RETARDO);
            if( !(input(PIN_A0) == 0 || input(PIN_A2) == 0)){
                return 1;
            }
        }
        for(j = 0; j < umbral; j++){
            PORTB = offset + 1;
            delay(RETARDO_SHORT);
            PORTB = offset + 4;
            delay(RETARDO_SHORT);
            PORTB = offset + 2;
            delay(RETARDO_SHORT);
            PORTB = offset + 7;
            delay(RETARDO_SHORT);
            PORTB = offset + 0;
            delay(RETARDO_SHORT);
            PORTB = offset + 7;
            delay(RETARDO_SHORT);
            PORTB = offset + 0;
            delay(RETARDO_SHORT);
            PORTB = offset + 6;
            delay(RETARDO_SHORT);
            PORTB = offset + 0;
            delay(RETARDO_SHORT);
            PORTB = offset + 3;
            delay(RETARDO_SHORT);
            PORTB = offset + 0;
            delay(RETARDO_SHORT);
            PORTB = offset + 5;
            delay(RETARDO_SHORT);
            PORTB = offset + 0;
            delay(RETARDO_SHORT);
            if( !(input(PIN_A0) == 0 || input(PIN_A2) == 0)){
                return 1;
            }
        }
    }
}

void delay(long time){
    long i;
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
    for(i = 0; i < time; i++);
}
